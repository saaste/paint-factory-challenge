﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaintFactoryChallenge.Libraries;

namespace PaintFactoryChallenge
{
    public class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("Usage: PaintFactoryChallenge.exe test-data.txt");
                return;
            }

            AppDomain.CurrentDomain.UnhandledException += ExceptionHandler;

            List<TestCase> testCases = TestCase.ReadFromFile(args[0]);
            for (int testCaseNumber = 1; testCaseNumber <= testCases.Count; testCaseNumber++)
            {
                Console.Write("Case #");
                Console.Write(testCaseNumber);
                Console.Write(": ");
                Console.WriteLine(testCases[testCaseNumber - 1].Run());
            }
            Console.WriteLine();
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }

        private static void ExceptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine();
            Console.WriteLine("Exception:");
            Console.WriteLine(e.ExceptionObject.ToString());
            Environment.Exit(1);

        }
    }
}
