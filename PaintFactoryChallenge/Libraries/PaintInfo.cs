﻿namespace PaintFactoryChallenge.Libraries
{
    /// <summary>
    /// Paint information
    /// </summary>
    public class PaintInfo
    {
        /// <summary>
        /// Enum indicating the type of the paint
        /// </summary>
        public enum ColorType
        {
            Undefined,
            Glossy,
            Matte
        }

        /// <summary>
        /// Color of the paint
        /// </summary>
        public int Color { get; set; }

        /// <summary>
        /// Type of the paint
        /// </summary>
        public ColorType Type { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="color">Color of the paint</param>
        public PaintInfo(int color)
        {
            this.Color = color;
            this.Type = ColorType.Undefined;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="color">Color of the paint</param>
        /// <param name="type">Type of the paint</param>
        public PaintInfo(int color, ColorType type)
            : this(color)
        {
            this.Type = type;
        }

    }
}
