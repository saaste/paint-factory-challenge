﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PaintFactoryChallenge.Libraries
{
    /// <summary>
    /// Test case
    /// </summary>
    public class TestCase
    {
        /// <summary>
        /// Colors available for the test case
        /// </summary>
        public List<PaintInfo> Paints { get; set; }

        /// <summary>
        /// Customer with their paint likes
        /// </summary>
        public List<Customer> Customers { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public TestCase()
        {
            Paints = new List<PaintInfo>();
            Customers = new List<Customer>();
        }

        /// <summary>
        /// Run the test and return string indicating how different
        /// paint colors should be prepared to satisfy customer needs.
        /// </summary>
        /// <returns>String indicating how paint should be prepared or IMPOSSIBLE if needs cannot be satisfied</returns>
        public string Run()
        {
            // List of paint type in order. 0 = glossy, 1 = matte
            List<int> paintTypes = new List<int>();

            // Define type for every color in test
            foreach (PaintInfo paintInfo in Paints)
            {

                // Get list of customer who have a like for the current color
                List<Customer> customersWithColor = (from c in Customers
                                                     where c.RequiresColor(paintInfo.Color)
                                                     select c).ToList();

                // Check every customer
                foreach (Customer customer in customersWithColor)
                {
                    // Customers requirement for the current color
                    PaintInfo customerRequirement = customer.GetPaintRequirement(paintInfo.Color);

                    // Customer accepts only one color so we MUST satisfy this need.
                    if (customer.PaintRequirements.Count == 1)
                    {
                        if (paintInfo.Type != PaintInfo.ColorType.Undefined && paintInfo.Type != customerRequirement.Type)
                        {
                            // Paint is already defined as other type so we cannot satisfy the customer need
                            // Stop the test and return IMPOSSIBLE immediately.
                            return "IMPOSSIBLE";
                        }
                        else
                        {
                            // Set type for the paint. This makes sure that if any other customer needs only this
                            // color in other type, the test returns IMPOSSIBLE
                            paintInfo.Type = customerRequirement.Type;
                        }
                    }
                }

                // Add paint type to the collection. If type is matte, set it to 1
                // Otherwise set it to 0 because glossy is cheaper ;)
                paintTypes.Add(paintInfo.Type == PaintInfo.ColorType.Matte ? 1 : 0);
            }

            // Build the return string
            return string.Join(" ", paintTypes);
        }

        public static List<TestCase> ReadFromFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException("Data file " + filePath + " does not exist!");
            }

            List<TestCase> testCases = new List<TestCase>();
            string[] data = File.ReadAllLines(filePath);

            int testCount = int.Parse(data[0]);
            int currentLine = 0;

            for (int testNumber = 0; testNumber < testCount; testNumber++)
            {
                // New test case
                TestCase test = new TestCase();

                // Get color options and set their type as undefined
                int colorOptions = int.Parse(data[currentLine + 1]);
                for (int i = 1; i <= colorOptions; i++)
                {
                    test.Paints.Add(new PaintInfo(i));
                }

                // Get customer count for the current test
                int customerCount = int.Parse(data[currentLine + 2]);

                // Skip to customer definitions
                currentLine += 2;

                for (int customerLineNumber = 1; customerLineNumber <= customerCount; customerLineNumber++)
                {
                    Customer customer = new Customer();

                    string customerLine = data[currentLine + customerLineNumber];
                    int customerColorCount = int.Parse(customerLine.Substring(0, 1));
                    for (int i2 = 0; i2 < customerColorCount; i2++)
                    {
                        int index = 2 + (i2 * 4);
                        int color = int.Parse(customerLine.Substring(index, 1));
                        PaintInfo.ColorType colorType = int.Parse(customerLine.Substring(index + 2, 1)) == 1 ? PaintInfo.ColorType.Matte : PaintInfo.ColorType.Glossy;

                        customer.PaintRequirements.Add(new PaintInfo(color, colorType));
                    }

                    test.Customers.Add(customer);
                }

                testCases.Add(test);
                currentLine += customerCount;

            }

            return testCases;
        }
    }
}
