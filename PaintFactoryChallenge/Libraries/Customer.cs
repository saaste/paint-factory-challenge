﻿using System.Collections.Generic;
using System.Linq;

namespace PaintFactoryChallenge.Libraries
{
    /// <summary>
    /// Customer
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// Paint requirements
        /// </summary>
        public List<PaintInfo> PaintRequirements { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public Customer()
        {
            PaintRequirements = new List<PaintInfo>();
        }

        /// <summary>
        /// Get paint requirement by color
        /// </summary>
        /// <param name="color">Color</param>
        /// <returns>Paint requirement</returns>
        public PaintInfo GetPaintRequirement(int color)
        {
            PaintInfo requirement = (from pr in PaintRequirements
                                     where pr.Color == color
                                     select pr).FirstOrDefault();
            return requirement;
        }

        /// <summary>
        /// Checks if customer requires the color
        /// </summary>
        /// <param name="color">Color</param>
        /// <returns>Boolean indicating if customer requires the color</returns>
        public bool RequiresColor(int color)
        {
            bool requiresColor = (from pr in PaintRequirements
                                  where pr.Color == color
                                  select pr).Any();
            return requiresColor;
        }
    }
}
